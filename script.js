//range
var range = document.getElementById("range");
var output = document.getElementById("demo");
output.innerHTML = range.value;

range.oninput = function() {
  output.innerHTML = this.value;

  //class select
  function clearClass() {
    card1.classList.remove("border-primary");
    card1.firstElementChild.classList.remove("bg-primary")
    card2.classList.remove("border-primary" );
    card2.firstElementChild.classList.remove("bg-primary")
    card3.classList.remove("border-primary");
    card3.firstElementChild.classList.remove("bg-primary")
  }

  if (this.value >= 0 && this.value <= 10) {

    clearClass();
    card1.classList.add("border-primary");
    card1.firstElementChild.classList.add("bg-primary")

  } else if (this.value > 10 && this.value <= 20) {

    clearClass();
    card2.classList.add("border-primary");
    card2.firstElementChild.classList.add("bg-primary")

  } else {

    clearClass();
    card3.classList.add("border-primary");
    card3.firstElementChild.classList.add("bg-primary")

  }
  
} 


//infinite scroll
let page = 1;
        let isLoading = false;

        const gallery = document.getElementById("gallery");
        const loader = document.getElementById("loader");

        function fetchImages() {
            isLoading = true;
            loader.style.display = "block";

            fetch(`https://picsum.photos/v2/list?page=${page}&limit=12`)
                .then(response => response.json())
                .then(images => {
                    isLoading = false;
                    loader.style.display = "none";

                    images.forEach(image => {
                        const imgElement = document.createElement("img");
                        imgElement.src = image.download_url;
                        imgElement.alt = "Random Image";
                        imgElement.classList.add("image");
                        gallery.appendChild(imgElement);
                    });

                    page++;
                })
                .catch(error => {
                    isLoading = false;
                    console.error("Error fetching images: ", error);
                });
        }

        function handleScroll() {
            const { scrollTop, clientHeight, scrollHeight } = document.documentElement;

            if (scrollTop + clientHeight >= scrollHeight - 10 && !isLoading) {
                fetchImages();
            }
        }

        window.addEventListener("scroll", handleScroll);
        fetchImages(); // Initial load
